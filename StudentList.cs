﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace list
{
    #region Елементы структуры
    public class Node
    {
        public Node(char country, int course, double score)
        {
            Country = country;
            Course = course;
            Score = score;
        }
        public char Country
        {
            get;
            internal set;
        }
        public int Course
        {
            get;
            internal set;
        }
        public double Score
        {
            get;
            internal set;
        }
        public Node Next
        {
            get;
            internal set;
        }
        public Node Prev
        {
            get;
            internal set;
        }
        public int student_id
        {
            get;
            internal set;
        }
    }
    #endregion
    #region Список
    public class list
    {
        public Node _head;
        public Node _tail;
        private int Count = 0;

        #region Удаление списка
        public void Clear()
        {
            _head = null;
            _tail = null;
            Count = 0;
            Console.WriteLine("List has been deleted.");
        }
        #endregion
        #region Удаление первого элемента списка
        public void RemoveFirst()
        {
            if (Count == 0)
            {
                _head = _head.Next;
                Count--;

                if (Count == 0)
                {
                    _tail = null;

                }
                else
                {
                    _head.Prev = null;
                }
            }

        }
        #endregion
        #region Удаление последнего элемента очереди
        public void RemoveLast()
        {
            if (Count != 0)
            {
                if (Count == 1)
                {
                    _head = null;
                    _tail = null;
                }
                else
                {
                    _tail.Prev.Next = null;
                    _tail = _tail.Prev;
                }
                Count--;
            }

        }
        #endregion
        #region Добавление элемента в начало списка
        public void addFirst(char country, int course, double score)
        {
            Node node = new Node(country, course, score);
            Node temp = _head;
            _head = node;
            _head.Next = temp;
            _head.student_id = Count + 1;

            if (Count == 0)
            {
                _tail = _head;
            }
            else
            {
                temp.Prev = _head;
            }
            Count++;
        }
        #endregion
        #region Добавление элемента в конец списка 
        public void addLast(char country, int course, double score)
        {
            Node node = new Node(country, course, score);
            if (Count == 0)
            {
                _head = node;
            }
            else
            {
                _tail.Next = node;
                node.Prev = _tail;

            }
            _tail = node;
            _tail.student_id = Count + 1;
            Count++;

        }
        #endregion
        #region Поиск иноземных студентов отличников 
        public void FindStudent()
        {

            Node current = _head;

            Console.WriteLine("_________________________________");
            while (current != null)
            {
                
                if ((current.Country != 'U') && (current.Score > 4))
                {

                    Console.Write("-Студент гражданин страны: ");
                    Console.WriteLine(current.Country);
                    Console.WriteLine( );
                    Console.Write("-Учится на: ");
                    Console.Write(current.Course);
                    Console.WriteLine(" курсе.");
                    Console.WriteLine();
                    Console.Write("-Средний балл: ");
                    Console.WriteLine(current.Score);
                    Console.WriteLine();
                    Console.Write("-Id студента: ");
                    Console.WriteLine(current.student_id);
                    Console.WriteLine();
                    Console.WriteLine("_________________________________");


                }

                current = current.Next;
            }

        }


        #endregion
        #region Удаление указанного элемента 
        public bool Remove(char country, int course, double score)
        {
            Node previous = null;
            Node current = _head;

            while (current != null)
            {
                if (current.Country.Equals(country) && current.Course.Equals(course) && current.Score.Equals(score))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;
                        if (current.Next == null)
                        {
                            _tail = previous;
                        }
                        else
                        {
                            current.Next.Prev = previous;
                        }
                        Count--;
                    }
                    else
                    {
                        RemoveFirst();
                    }
                    return true;

                }
                previous = current;
                current = current.Next;
            }
            return false;
        }
        #endregion
        #region Вывод списка
        public void ShowList()
        {

            Node current = _head;

            Console.WriteLine("_________________________________");
            while (current != null)
            {

                

                    Console.Write("-Студент гражданин страны: ");
                    Console.WriteLine(current.Country);
                    Console.WriteLine();
                    Console.Write("-Учится на: ");
                    Console.Write(current.Course);
                    Console.WriteLine(" курсе.");
                    Console.WriteLine();
                    Console.Write("-Средний балл: ");
                    Console.WriteLine(current.Score);
                    Console.WriteLine();
                    Console.Write("-Id студента: ");
                    Console.WriteLine(current.student_id);
                    Console.WriteLine();
                    Console.WriteLine("_________________________________");


                

                current = current.Next;
            }

        }
        #endregion
        #region Удаление по ID
        public bool Remove_by_id(int id)
        {
            Node previous = null;
            Node current = _head;

            while (current != null)
            {
                if (current.student_id.Equals(id))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;
                        if (current.Next == null)
                        {
                            _tail = previous;
                        }
                        else
                        {
                            current.Next.Prev = previous;
                        }
                        Count--;
                    }
                    else
                    {
                        RemoveFirst();
                    }
                    return true;

                }
                previous = current;
                current = current.Next;
            }
            return false;
        }
        #endregion
        #region Сортировка по среднеднему баллу (возростание)
        public void SortList()
        {
           Node current = _head;
           Node data = null;
           Node current_next = current.Next;

            

            for (int i = 0; i < Count; i++)
            {
                current = _head;
                for (int j = 0; j < Count-1; j++)
                {
                    
                    if (current.Score.CompareTo(current.Next.Score)>0)
                    {
                        data = current.Next;
                        current.Next = current;
                        current = data;
                    }
                    current = current.Next;
                   
                }
            }

        }
        #endregion
    }
    #endregion
    #region Мейн программа
    class Program
    {
        static void Main(string[] args)
        {
            int flag = 0;
            list studList = new list { };
            while (flag != 11)
            {
                Console.WriteLine("Chose function: 1 - add student to head.");
                Console.WriteLine("Chose function: 2 - add student to tail.");
                Console.WriteLine("Chose function: 3 - delete first item.");
                Console.WriteLine("Chose function: 4 - delete last item.");
                Console.WriteLine("Chose function: 5 - search student.");
                Console.WriteLine("Chose function: 6 - delete list.");
                Console.WriteLine("Chose function: 7 - show list.");
                Console.WriteLine("Chose function: 8 - delete chosen item.");
                Console.WriteLine("Chose function: 9 - delete chosen item by ID.");
                Console.WriteLine("Chose function: 10 - sort list.");
                Console.WriteLine("Chose function: 11 - end program.");
                Console.Write("Your choise: ");
                try
                {
                    flag = Convert.ToInt32(Console.ReadLine());
                }
                catch(FormatException)
                {
                    Console.WriteLine("_______________________");
                    Console.WriteLine(" ");
                    Console.WriteLine("Wrong format!");
                    Console.WriteLine("_______________________");
                    Console.WriteLine(" ");
                }
            
                
                    if (flag == 1)
                    {
                        try
                        {
                            Console.Write("Enter student country: ");
                            char country = Convert.ToChar(Console.ReadLine());
                            Console.Write("Enter student course: ");
                            int course = Convert.ToInt32(Console.ReadLine());
                            Console.Write("Enter student score: ");
                            double score = Convert.ToDouble(Console.ReadLine());
                            studList.addFirst(country, course, score);
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("_______________________");
                            Console.WriteLine(" ");
                            Console.WriteLine("Wrong format!");
                            Console.WriteLine("_______________________");
                            Console.WriteLine(" ");
                        }
                    }
                    else if (flag == 2)
                    {
                        try
                        {
                            Console.Write("Enter student country: ");
                            char country = Convert.ToChar(Console.ReadLine());
                            Console.Write("Enter student course: ");
                            int course = Convert.ToInt32(Console.ReadLine());
                            Console.Write("Enter student score: ");
                            double score = Convert.ToDouble(Console.ReadLine());
                            studList.addLast(country, course, score);
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("_______________________");
                            Console.WriteLine(" ");
                            Console.WriteLine("Wrong format!");
                            Console.WriteLine("_______________________");
                            Console.WriteLine(" ");
                        }
                    }
                    else if (flag == 3)
                    {
                        studList.RemoveFirst();
                    }
                    else if (flag == 4)
                    {
                        studList.RemoveLast();
                    }
                    else if (flag == 5)
                    {
                        studList.FindStudent();

                    }
                    else if (flag == 6)
                    {
                        studList.Clear();
                    }
                    else if (flag == 7)
                    {
                        studList.ShowList();
                    }
                    else if (flag == 8)
                    {
                        try
                        {
                            Console.Write("Enter student country: ");
                            char country = Convert.ToChar(Console.ReadLine());
                            Console.Write("Enter student course: ");
                            int course = Convert.ToInt32(Console.ReadLine());
                            Console.Write("Enter student score: ");
                            double score = Convert.ToDouble(Console.ReadLine());
                            studList.Remove(country, course, score);
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("_______________________");
                            Console.WriteLine(" ");
                            Console.WriteLine("Wrong format!");
                            Console.WriteLine("_______________________");
                            Console.WriteLine(" ");
                        }
                    catch(NullReferenceException)
                    {
                        Console.WriteLine("Item doesn't exist.");
                    }

                    }
                    else if (flag == 9)
                    {
                        try
                        {
                            Console.Write("Enter Id to remove: ");
                            int id = Convert.ToInt32(Console.ReadLine());
                            studList.Remove_by_id(id);
                            Console.WriteLine("________________________________");
                            Console.WriteLine("Item removed.");
                            Console.WriteLine("________________________________");
                            Console.WriteLine(" ");
                        }
                        catch (NullReferenceException)
                        {
                            Console.WriteLine("Item doesn't exist.");
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("_______________________");
                            Console.WriteLine(" ");
                            Console.WriteLine("Wrong format!");
                            Console.WriteLine("_______________________");
                            Console.WriteLine(" ");
                        }
                    }
                    else if (flag == 10)
                    {
                        studList.SortList();
                    }
                    else
                    {
                        Console.WriteLine(" ");
                        Console.WriteLine("Error!");
                        Console.WriteLine("________________________________");
                        Console.WriteLine(" ");
                        Console.WriteLine("Try again.");
                        Console.WriteLine(" ");
                    }
                }
                
            }

    }
    #endregion
}






















































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































